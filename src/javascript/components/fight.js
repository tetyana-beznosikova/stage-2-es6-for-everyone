import { controls } from '../../constants/controls';
import { setHealthBarWidth } from './arena';

const pressedKeysMap = new Map();

export async function fight(firstFighter, secondFighter) {
	initPressedKeys();
  	document.addEventListener('keydown', onKeyDown);
  	document.addEventListener('keyup', onKeyUp);
	firstFighter.player = 'One';
	firstFighter.position = 'left';
	firstFighter.doubleHit = true;
	secondFighter.player = 'Two';
	secondFighter.position = 'right';
	secondFighter.doubleHit = true;	

	let firstFighterHealth = firstFighter.health;
	let secondFighterHealth = secondFighter.health;


  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let doubleHitCounter = 0;
    let timeOutPeriod = 500;

	let timerId = setTimeout(function checkHealth() {
		let damageOne = getDamage(secondFighter, firstFighter);
		let damageTwo = getDamage(firstFighter, secondFighter);
		if (damageOne > 0){
			firstFighterHealth = firstFighterHealth - damageOne;
			let width = (100 * firstFighterHealth) / firstFighter.health;
			setHealthBarWidth(firstFighter.position, width);
		}
		if (damageTwo > 0){
			secondFighterHealth = secondFighterHealth - damageTwo;
			let width = (100 * secondFighterHealth) / secondFighter.health;			
			setHealthBarWidth(secondFighter.position, width);
		}		
    	
    	doubleHitCounter += timeOutPeriod;
    	if (doubleHitCounter >= 10000){
			doubleHitCounter = 0;
			firstFighter.doubleHit = true;
			secondFighter.doubleHit = true;	
    	}

    	if (firstFighterHealth <= 0 || secondFighterHealth <= 0){
		  	const winner = firstFighterHealth <= 0 ? secondFighter: firstFighter;
			clearTimeout(timerId);
		    resolve(winner); 
    	} else {
	    	timerId = setTimeout(checkHealth, timeOutPeriod);
    	}	
	}, timeOutPeriod)
  });
}

export function getDamage(attacker, defender) {
	let doubleHit = getDoubleHitPower(attacker);
	if ( doubleHit > 0){
		return doubleHit;
	}
	let damage = getHitPower(attacker) - getBlockPower(defender);
  	return (damage > 0) ? damage : 0;
}

export function getDoubleHitPower(fighter) {
  if (fighter.doubleHit && isPressedDoubleHitKeys(fighter)){
	  const { attack } = fighter;
	  const criticalHitChance = Math.random() + 1;
	  fighter.doubleHit = false;
	  return 2 * attack * criticalHitChance;  	
  } 
  return 0; 
}

export function getHitPower(fighter) {
  if (isPressedBlockKey(fighter) || !isPressedAttackKey(fighter)){
  	return 0;
  }  
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  if (!isPressedBlockKey(fighter)){
  	return 0;
  }
  const { defense } = fighter;
  const criticalHitChance = Math.random() + 1;
  return defense * criticalHitChance;
}

function isPressedAttackKey(fighter){
	const controlsKey = `Player${fighter.player}Attack`;
	const key = controls[controlsKey];
	return pressedKeysMap.get(key);
}

function isPressedBlockKey(fighter){
	const controlsKey = `Player${fighter.player}Block`;
	const key = controls[controlsKey];	
	return pressedKeysMap.get(key);
}

function isPressedDoubleHitKeys(fighter){
	const controlsKey = `Player${fighter.player}CriticalHitCombination`;
	const keys = controls[controlsKey];	
	return Array.from(keys,  key => pressedKeysMap.get(key)).every(elem => elem);
}

function initPressedKeys(){
	Object.keys(controls).forEach(key => {
		const item = controls[key];
		if (Array.isArray(item)){
			item.forEach(subKey => {pressedKeysMap.set(subKey, false);});
		} else {
			pressedKeysMap.set(item, false);
		}
	});
}

function onKeyDown(event){
	if(pressedKeysMap.has(event.code)){
		pressedKeysMap.set(event.code, true);
	}
}

function onKeyUp(event){
	if(pressedKeysMap.has(event.code)){
		pressedKeysMap.set(event.code, false);
	}
}