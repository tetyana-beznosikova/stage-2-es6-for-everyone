import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  // TODO: #1
  if (fighter){
    const { source, name } = fighter;
    const imageElement = createFighterImage(fighter);
    const fighterInfoElement = createFighterInfo(fighter);
    fighterElement.append(imageElement);
    fighterElement.append(fighterInfoElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterInfo(fighter) {
  const { name, attack, defense, health } = fighter;  
  const infoElement = createElement({
    tagName: 'div',
    className: `fighter-preview___info`,
  });  
  const nameElement = createElement({ tagName: 'span'});
  nameElement.innerText = 'Name: ' + name;

  const attackElement = createElement({ tagName: 'span'});
  attackElement.innerText = 'Attack: ' + attack;  

  const defenseElement = createElement({ tagName: 'span'});
  defenseElement.innerText = 'Defense: ' + defense;

  const healthElement = createElement({ tagName: 'span'});
  healthElement.innerText = 'Health: ' + health;

  infoElement.append(nameElement);
  infoElement.append(attackElement);
  infoElement.append(defenseElement);
  infoElement.append(healthElement);

  return infoElement;
}

