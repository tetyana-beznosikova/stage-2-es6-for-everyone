import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function 
  const bodyElement = createFighterImage(fighter);
  const { name } = fighter;  
  const title = 'Winner - ' + name;
  showModal({ title, bodyElement });
}
